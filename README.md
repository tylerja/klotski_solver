# klotski_solver

This tool solves klotski puzzles. See `klotski_tools.py` for algorithmic code and klotski_demo.ipynb for example usage.

## TODO

Improve `get_move_changed_tiles` and related code to make proper use of `tile.x` and `tile.y` and reprofile code.
