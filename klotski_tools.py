from copy import deepcopy
from queue import *
from time import perf_counter
from string import ascii_lowercase as alphabet

CMAP = {
    'a': '\33[107m a ',
    'b': '\33[41m b ',
    'c': '\33[42m c ',
    'd': '\33[43m d ', 
    'e': '\33[44m e ',
    'f': '\33[45m f ',
    'g': '\33[46m g ',
    'h': '\33[47m h ',
    'i': '\33[100m i ',
    'j': '\33[103m j ',
    'k': '\33[102m k ',
    'l': '\33[106m l ', 
    'm': '\33[104m m ',
    'n': '\33[105m n ',
    'o': '\33[100m o ',
    '0': '\33[7m   '
}

EMPTY = '000'

class Vertex:
    def __init__(self, id_, board, pred=None):
        self.id = id_
        self.board = board
        self.pred = pred
        self.distance = pred.distance + 1 if pred is not None else 0


class Tile:
    def __init__(self, width=0, height=0, label='0', x=None, y=None):
        self.width = width
        self.height = height
        self.label = label
        # Top-left coordinate
        self.x = x
        self.y = y
    
    def __str__(self):
        """A string representing the tile."""
        return str(self.width) + str(self.height) + str(self.label)


def print_board(board):
    """Print out a picture of the board."""
    for y in range(len(board)):
        line_str = ''
        for x in range(len(board[y])):
            square_str = f'{CMAP[board[y][x][2]]}\033[0m'
            if square_str == '0':
                square_str = '\x1b[31m0\x1b[0m'
            line_str += square_str + ' '
        print(line_str)

def board_to_int(board):
    """Return a key corresponding to a given board."""
    board_str = ''
    for y in range(len(board)):
        for x in range(len(board[y])):
            board_str += str(board[y][x][0:2])
    return int(board_str)

def in_bounds(board, x, y):
    """Check if coordinate x, y is in bounds."""
    return 0 <= x < len(board[0]) and 0 <= y < len(board)

def convert_board(simple_board):
    """Take a user input board and return a labeled board and tile set."""
    tiles = []
    board = simple_board
    for j in range(len(board)):
        for i in range(len(board[j])):
            square_str = board[j][i]
            if len(square_str) == 3:
                # Square already accounted for
                continue
            if square_str == '00':
                board[j][i] = EMPTY
                continue
            x_span = int(square_str[0])
            y_span = int(square_str[1])
            tile_letter = alphabet[len(tiles)]
            tiles.append(Tile(x_span, y_span, tile_letter))
            for dy in range(y_span):
                for dx in range(x_span):
                    board[j+dy][i+dx] += tile_letter
    return board, tiles

def get_move_changed_tiles(board, tile, dx, dy):
    """Return a board achieved by moving tile in direction (dx,dy)."""
    old_squares = set()  # Squares that are being moved from
    new_squares = set()  # Squares that are being moved to
    # Find squares belonging to the tile (TODO track position instead of looping)
    for y in range(len(board)):
        for x in range(len(board[y])):
            if board[y][x] == str(tile):
                # Check if the tile can move
                if not in_bounds(board, x + dx, y + dy) or board[y + dy][x + dx] not in [EMPTY, str(tile)]:
                    return [], []  # Can't move to non-existent or non-empty square
                old_squares.add((x, y))
                new_squares.add((x + dx, y + dy))
    # Construct the board being moved to
    empty_squares = old_squares - new_squares
    fill_squares = new_squares - old_squares
    return empty_squares, fill_squares

def move_tile_in_place(board, tile, empty_squares, fill_squares):
    """Set a boards squares accordingly."""
    for sq_x, sq_y in fill_squares:
        board[sq_y][sq_x] = str(tile)
    for sq_x, sq_y in empty_squares:
        board[sq_y][sq_x] = EMPTY

def find_reachable_vertices(head_vertex, from_vertex, tile, existing_vertex_ids, reachable_vertices=[]):
    """Return `reachable_vertices` as a list of reachable vertices/boards."""
    reachable_vertices.append(from_vertex)
    for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
        # Get squares that need to change on board
        empty_squares, fill_squares = get_move_changed_tiles(from_vertex.board, tile, dx, dy)
        # If none, continue
        if len(empty_squares) == 0:
            continue
        # Change squares in place
        move_tile_in_place(from_vertex.board, tile, empty_squares, fill_squares)
        # Get ID
        to_vertex_id = board_to_int(from_vertex.board)
        # If ID is new, deepcopy board (else use existing)
        is_new_vertex = to_vertex_id not in existing_vertex_ids
        is_old_vertex_new_tile = not is_new_vertex and str(tile)[:2] not in existing_vertex_ids[to_vertex_id]
        if is_new_vertex or is_old_vertex_new_tile:
            # If the move is possible and reaches a board not reached yet
            to_board = from_vertex.board
            if is_new_vertex:
                # Only want to run deepcopy for new boards to save time
                to_board = deepcopy(to_board)
                existing_vertex_ids[to_vertex_id] = {}
            to_vertex = Vertex(to_vertex_id, to_board, head_vertex)
            existing_vertex_ids[to_vertex_id][str(tile)[:2]] = True
            find_reachable_vertices(head_vertex, to_vertex, tile, existing_vertex_ids, reachable_vertices)  # Keep trying to move
        # Move the tile back
        move_tile_in_place(from_vertex.board, tile, fill_squares, empty_squares)

def is_winner(b, winning_board):
    """Return true if a board `b` is a winner."""
    for y in range(len(b)):
        for x in range(len(b[y])):
            if winning_board[y][x] != EMPTY and winning_board[y][x] != b[y][x]:
                return False
    return True
    
def bfs_jit(start_board, tiles, winning_board, debug=False):
    """Search for a winning solution using a BFS, creating the graph just in time."""
    start_time = perf_counter()
    existing_vertex_ids = {}
    start = Vertex(board_to_int(start_board), start_board, None)
    existing_vertex_ids[start.id] = {}
    for tile in tiles:
        existing_vertex_ids[start.id][str(tile)[:2]] = True
    vertex_queue = Queue()
    vertex_queue.put(start)
    prev_distance = -1
    # While searching for the winner
    while vertex_queue.qsize() > 0:
        current_vertex = vertex_queue.get()
        if current_vertex.distance != prev_distance:
            prev_distance = current_vertex.distance
            if debug:
                print(f'Depth is now {prev_distance} after visiting {len(existing_vertex_ids)} boards in {perf_counter()-start_time:.3f}s')
        # Find neighbors
        for tile in tiles:
            neighbor_vertices = []
            find_reachable_vertices(current_vertex, current_vertex, tile, existing_vertex_ids, neighbor_vertices)
            # Add edges between boards (but none from a board to itself)
            for neighbor_vertex in neighbor_vertices:
                if neighbor_vertex.id != current_vertex.id:
                    # If a winner, we're done
                    if is_winner(neighbor_vertex.board, winning_board):
                        print(f'Finished at depth {neighbor_vertex.distance} after visiting {len(existing_vertex_ids)} boards in {perf_counter()-start_time:.3f}s')
                        return neighbor_vertex
                    # If not a winner, add it to the queue to look at
                    vertex_queue.put(neighbor_vertex)
    print('Error... no winner found')
    return None

def traverse(winning_vertex):
    """Traverse back up the graph to get a set of tile moves to get to the fastest solution."""
    board_moves = []
    vertex = winning_vertex
    board_moves.append(vertex.board)
    while vertex.pred is not None:
        vertex = vertex.pred
        board_moves.append(vertex.board)
    board_moves.reverse()
    return board_moves


